package com.example.parcial2_barria_martinez_vasquez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class EditarActivity extends AppCompatActivity {

    EditText n, p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        this.InicializarControles();
    }

    private void InicializarControles(){
        n = (EditText)findViewById(R.id.nomId);
        p = (EditText)findViewById(R.id.PassId);
    }

    public void editarNombre(View view){
        Intent i = getIntent();
        String txtnombre = n.getText().toString();
        if(txtnombre.equals("")){
            Toast.makeText(this, "Debe insertar un nombre para cambiarlo", Toast.LENGTH_SHORT).show();
        }
        else{
            if(i.getStringExtra("rolEdit").equals("Estudiante")){
                SharedPreferences pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("nombre",txtnombre);
                editor.commit();

                Toast.makeText(this, "Nombre Cambiado", Toast.LENGTH_SHORT).show();

            }
            else{
                SharedPreferences pref = getSharedPreferences("Profesor", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("nombre",txtnombre);
                editor.commit();

                Toast.makeText(this, "Nombre Cambiado", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void editarContra(View view){
        Intent i = getIntent();
        String txtpass= p.getText().toString();
        if(txtpass.equals("")){
            Toast.makeText(this, "Debe insertar una contraseña para cambiarla", Toast.LENGTH_SHORT).show();
        }
        else{
            if(i.getStringExtra("rolEdit").equals("Estudiante")){
                SharedPreferences pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("passw",txtpass);
                editor.commit();

                Toast.makeText(this, "Contraseña cambiada", Toast.LENGTH_SHORT).show();

            }
            else{
                SharedPreferences pref = getSharedPreferences("Profesor", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("passw",txtpass);
                editor.commit();

                Toast.makeText(this, "Contraseña cambiada", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
