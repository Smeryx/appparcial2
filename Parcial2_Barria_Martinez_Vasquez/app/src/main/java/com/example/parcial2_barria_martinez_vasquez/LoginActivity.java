package com.example.parcial2_barria_martinez_vasquez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class LoginActivity extends AppCompatActivity {

    EditText Ced,Pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.InicializaUsuario();
        this.InicializarControles();
        this.InicializarMaterias();


    }

    public void InicializarMaterias(){
        try{
            OutputStreamWriter escribir = new OutputStreamWriter(openFileOutput("Materias.txt",MODE_PRIVATE));
            escribir.write("Física I,Semestre I,N;");
            escribir.write("Física II,Semestre II,N;");
            escribir.write("Cálculo I,Semestre I,N;");
            escribir.write("Cálculo II,Semestre II,N;");
            escribir.write("Cálculo III,Semestre II,N;");
            escribir.write("Programación I,Semestre I,N;");
            escribir.write("Programación II,Semestre II,N;");
            escribir.write("Globalización de Software,Semestre I,N;");
            escribir.write("Lógica Computacional,Semestre II,N;");
            escribir.write("Química ,Semestre I,N;");

            escribir.close();
        }
        catch(Exception e){
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }



    }
    private void InicializarControles(){
        Ced = (EditText)findViewById(R.id.CedId);
        Pass = (EditText)findViewById(R.id.PassId);


    }


    public void InicializaUsuario(){


        SharedPreferences pref;

        pref = getSharedPreferences("Profesor", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("nombre","Denzel Crocker");
        editor.putString("cedula","4-986-654");
        editor.putString("passw","whatever");
        editor.putString("rol","Profesor");

        editor.commit();

        pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("nombre","Timmy Tunner");
        editor.putString("cedula","2-986-654");
        editor.putString("passw","fairy");
        editor.putString("rol","Estudiante");

        editor.commit();


    }

    public void Ingresar(View view){

        try{
            String CedulaEscrita = Ced.getText().toString();
            String PassEscrita = Pass.getText().toString();
            SharedPreferences pref = null;
            pref = getSharedPreferences("Profesor", MODE_PRIVATE);
            if(CedulaEscrita.equals(pref.getString("cedula",""))&&PassEscrita.equals(pref.getString("passw",""))){
                Intent i = new Intent(this, MainActivity.class);
                i.putExtra("rolereg",pref.getString("rol","N/M"));
                startActivity(i);
                Toast.makeText(this, "Logueado Profesor", Toast.LENGTH_SHORT).show();

            } else {
                pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
                if(CedulaEscrita.equals(pref.getString("cedula",""))&&PassEscrita.equals(pref.getString("passw",""))){
                    Intent i = new Intent(this, MainActivity.class);
                    i.putExtra("rolereg",pref.getString("rol","N/M"));
                    startActivity(i);
                    Toast.makeText(this, "Logueado Estudiante", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "Cedula o Contraseña incorrecta ", Toast.LENGTH_SHORT).show();
                }

            }



        }
        catch (Exception e){
            Toast.makeText(this, "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
}
