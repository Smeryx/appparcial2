package com.example.parcial2_barria_martinez_vasquez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parcial2_barria_martinez_vasquez.Adaptadores.HistorialListViewAdapter;
import com.example.parcial2_barria_martinez_vasquez.entidades.Historial;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EstudianteActivity extends AppCompatActivity {
     ListView lstHistorial;
     TextView Nombre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiante);

        this.InicializarControles();
        this.LoadList();
        this.InicializarDatos();
    }

    @Override
    protected void onResume(){
        super.onResume();

        setContentView(R.layout.activity_estudiante);

        this.InicializarControles();
        this.LoadList();
        this.InicializarDatos();
    }

    private void InicializarControles(){
        lstHistorial = (ListView)findViewById(R.id.lvEstudiante);
        Nombre = (TextView)findViewById(R.id.lblEstudiante);
    }

    private void InicializarDatos(){
        SharedPreferences pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
        Nombre.setText(pref.getString("nombre","None"));
    }

    private int obtenerImagen(String materia){
        int img = R.drawable.notfound;
        switch(materia){
            case "Física I":
            case "Física II":
                img = R.drawable.atom;
                break;
            case "Cálculo I":
            case "Cálculo II":
            case "Cálculo III":
                img = R.drawable.calculadora;
                break;
            case "Programación I":
            case "Programación II":
                img = R.drawable.code;
                break;
            case "Lógica Computacional":
                img = R.drawable.pc;
                break;
            case "Globalización de Software":
                img = R.drawable.mundo;
                break;
            case "Química":
                img = R.drawable.chemical;
                break;
        }
        return img;
    }

    private int obtenerImagenNota(String nota){
        int img = R.drawable.notfound;
        switch(nota){
            case "A":
            case "B":
            case "C":
                img = R.drawable.check;
                break;
            case "D":
            case "F":
                img = R.drawable.wrong;
                break;
            case "N":
                img = R.drawable.circle;
                break;
        }
        return img;
    }

    private void LoadList(){
        try{
            List<Historial> notas = new ArrayList<Historial>();

            BufferedReader leer = new BufferedReader(new InputStreamReader(openFileInput("Materias.txt")));
            String historial = leer.readLine();
            leer.close();

            String[] materias = historial.split(";");
            String[] infoMateria;

            for(int x=0; x<materias.length; x++){
                infoMateria = materias[x].split(",");
                notas.add(new Historial(obtenerImagen(infoMateria[0]), infoMateria[0], infoMateria[1], infoMateria[2], obtenerImagenNota(infoMateria[2])));
            }

            HistorialListViewAdapter adapter = new HistorialListViewAdapter(getApplicationContext(), notas);

            lstHistorial.setAdapter(adapter);
        }
        catch(Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void Editar(View view){
        try{
            Intent i = new Intent(this, EditarActivity.class);
            SharedPreferences pref = getSharedPreferences("Estudiante", MODE_PRIVATE);
            i.putExtra("rolEdit",pref.getString("rol","None"));
            startActivity(i);
        }
        catch(Exception e){
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
