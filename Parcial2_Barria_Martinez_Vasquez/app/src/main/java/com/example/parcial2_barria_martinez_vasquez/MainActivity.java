package com.example.parcial2_barria_martinez_vasquez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioGroup Op;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.InicializarControles();
    }

    private void InicializarControles(){
        Op = (RadioGroup) findViewById(R.id.rgOpciones);
        Intent i = getIntent();
        if(i.getStringExtra("rolereg")!=null){
            String txtrbtRol = "Sitio de "+i.getStringExtra("rolereg");
            ((RadioButton)Op.getChildAt(0)).setText(txtrbtRol);
        }
        else{
            String txtrbtRol = "NO ROLE";
            ((RadioButton)Op.getChildAt(0)).setText(txtrbtRol);
        }

    }

    public void Entrar (View view){

        try {
            switch (Op.getCheckedRadioButtonId()) {
                case R.id.rbtRol:
                    Intent i = getIntent();

                    if(i.getStringExtra("rolereg").equals("Estudiante")){
                        Intent m = new Intent(this, EstudianteActivity.class);
                        startActivity(m);
                    }
                    else{
                        Intent m = new Intent(this, ProfesorActivity.class);
                        startActivity(m);
                    }
                    break;
                case R.id.rbtUtp:
                    Intent n = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.utp.ac.pa/"));
                    startActivity(n);
                    break;
                case R.id.rbtEcampus:
                    Intent x = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ecampus.utp.ac.pa/moodle/"));
                    startActivity(x);
                    break;

            }

        }  catch (Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
}
