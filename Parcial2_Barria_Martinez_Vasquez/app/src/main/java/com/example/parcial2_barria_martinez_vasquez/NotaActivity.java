package com.example.parcial2_barria_martinez_vasquez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.parcial2_barria_martinez_vasquez.entidades.Historial;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class NotaActivity extends AppCompatActivity {

    RadioGroup Notas;
    Spinner Materias;
    String materiaSelect = "None";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);

        this.InicializarControles();
    }

    private void InicializarControles(){
        Notas = (RadioGroup)findViewById(R.id.rgNotas);
        Materias = (Spinner)findViewById(R.id.materiaNotaId);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, LoadSpinner());
        Materias.setAdapter(adapter);

        AttachEventToSpinner();
    }

    private List<String> LoadSpinner(){

        List<String> list = new ArrayList<String>();
        list.add("Seleccionar Materia");
        try{


            BufferedReader leer = new BufferedReader(new InputStreamReader(openFileInput("Materias.txt")));
            String historial = leer.readLine();
            leer.close();

            String[] materias = historial.split(";");
            String[] infoMateria;

            for(int x=0; x<materias.length; x++){
                infoMateria = materias[x].split(",");
                if(infoMateria[2].equals("N")){
                    list.add(infoMateria[0]);
                }
            }
        }
        catch(Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }


        return list;
    }

    private void AttachEventToSpinner(){
        Materias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String materia = parent.getItemAtPosition(position).toString();
                if(position != 0){
                    materiaSelect = materia;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    public void AddNota(View view){
        try {
            String nota = "N";
            switch (Notas.getCheckedRadioButtonId()) {
                case R.id.rbtAId:
                    nota = "A";
                    break;
                case R.id.rbtBId:
                    nota = "B";
                    break;
                case R.id.rbtCId:
                    nota = "C";
                    break;
                case R.id.rbtDId:
                    nota = "D";
                    break;
                case R.id.rbtFId:
                    nota = "F";
                    break;
            }
            if(nota!="N"){
                if(materiaSelect.equals("None")){
                    Toast.makeText(this, "Debe seleccionar una materia", Toast.LENGTH_SHORT).show();
                }
                else {
                    BufferedReader leer = new BufferedReader(new InputStreamReader(openFileInput("Materias.txt")));
                    String historial = leer.readLine();
                    leer.close();

                    String[] materias = historial.split(";");
                    String[] infoMateria;

                    OutputStreamWriter escribir = new OutputStreamWriter(openFileOutput("Materias.txt",MODE_PRIVATE));
                    for (int x = 0; x < materias.length; x++) {
                        infoMateria = materias[x].split(",");
                        if (infoMateria[0].equals(materiaSelect)) {
                            materias[x] = materias[x].replace(",N", "," + nota);
                        }
                        escribir.write(materias[x]+";");
                    }
                    escribir.close();
                    Toast.makeText(this, "Nota añadida", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            else{
                Toast.makeText(this, "Debe seleccionar una nota", Toast.LENGTH_SHORT).show();
            }

        }
        catch(Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
